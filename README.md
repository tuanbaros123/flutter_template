## Introduce  
This is a template for Flutter project use [Flutter Bloc v7.3.0](https://pub.dev/packages/flutter_bloc) library  
  
Before you continue, ensure you meet the following requirements:  
- [Cookiecutter](https://cookiecutter.readthedocs.io/en/1.7.2/installation.html)  
  
## Install  
First, run command below:  
```  
 cookiecutter https://gitlab.com/tuanbaros123/flutter_template.git --checkout develop-bloc-null-safety
 ```  
Then input some information in terminal. You will be prompted to enter a bunch of project config values. Press enter if you don't want to change
```
"app_name": default is MyApp,
"app_id": default is "com.flutter.sample",
"android_app_id": default is "com.flutter.sample",
"flutter_package_name": default is "sample",
"android_package_dir": by default it is created according to app_id (should not be changed).
```
Next, cookiecutter will generate a project from the template, using the values that you entered. It will be placed in your current directory.

## Environments
There are 3 environments in app:  **Develop, Staging and Production.**

To change, and some field for each environment, you must change in .env file.

> Develop:  **.env**

> Staging :  **.env.staging**

> Production :  **.env.production**

Example .env file:
```    
    APP_NAME=[DEV] Example  
    APP_ID=com.flutter.sample-dev  
    ANDROID_APP_ID=com.flutter.sample_dev  
    VER_CODE=1  
    VER_NAME=1.0.0  
    KEY_STORE=../keystores/develop/keystore.properties
``` 
##  Android Keystore
Config develop/staging/production keystore properties and file keystore in the corresponding directory.

## Run and build
Run `pod install` in `ios` directory before build/run on iOS device.

Edit configurations

![configurations](https://i.imgur.com/W0MDB40.png)

Change *Build flavor*

Enter *develop*, *staging* or *production* then you can run or build like other Flutter projects.

![Imgur](https://i.imgur.com/EZT8q0Z.png)

## Multi language
Add supported locales. [Supported languages in flutter](https://github.com/flutter/flutter/tree/master/packages/flutter_localizations/lib/src/l10n)
- In `app.dart`
```dart 
supportedLocales: [const Locale('vi', ''), const Locale('en', '')]
```
- In `localizations.dart`
```dart 
bool isSupported(Locale locale) => ['en', 'vi'].contains(locale.languageCode);
```
- In assets/strings create json file contains translation. Example `en.json`
```json
   {
     "example": "Example"
   }
```
Use
```dart 
Language.of(context).getText("example"); //==> Example
```
## Call api
- GET:

Example:
```dart
final network = Network();
network.get(url: "http://dummy.restapiexample.com/api/v1/employees", params: {});
```
- POST: 

Example:
```dart
final network = Network();
Map data = {"name":"test","salary":"123","age":"23"};
network.post(url: "http://dummy.restapiexample.com/api/v1/create", body: data);
```
## Flutter Bloc (Cubit)
- Create State:
```dart
class InitState extends BaseState {}

class LoadingState extends BaseState {}

class LoadedState<T> extends BaseState {
  final T data;

  LoadedState({@required this.data}) : assert(data != null);

  @override
  List<Object> get props => [data];
}

class ErrorState<T> extends BaseState {
  final T data;

  ErrorState({@required this.data}) : assert(data != null);

  @override
  List<Object> get props => [data];
}
```
- Create cubit:
```dart
class HomeCubit extends Cubit<BaseState> {
  final HomeRepository homeRepository;

  HomeCubit({HomeRepository homeRepository}) : this.homeRepository = homeRepository, super(InitState());

  getData() async {
    emit(LoadingState());
    try {
      //do something
      final data = await homeRepository.getData();
      emit(LoadedState<String>(data)) ;
    } catch (e) {
      emit(ErrorState(e));
    }
  }
}
```
- Use bloc:
```dart
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) => HomeCubit(homeRepository: HomeRepository())..getData(),
      child: Scaffold(
        body: Center(
          child: BlocBuilder<HomeCubit, BaseState>(
            builder: (context, state) {
              if (state is LoadingState) return CircularProgressIndicator();
              if (state is LoadedState) return Text(state.data);
              return Text(Language.of(context).getText("example"));
            },
          ),
        ),
      ),
    );
  }
}
```
Access [Flutter bloc](https://pub.dev/packages/flutter_bloc) for more information. 

## Scale size
- Init
```dart
  ScaleSize.init(context, designWidth: 360);
```
- Get size
```dart
  ScaleSize.get(10)  // or 10.sw;
```

## Generate launcher icon (ic_launcher for Android, app_icon for iOS)
- Copy an icon which you want to make ic_launcher and app_icon to app_icon folder and change name to app_icon.png 
- Run command below
```shell script
  flutter pub run flutter_launcher_icons:main
```
Access [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons) for more information.

## Libs use in this template
- [equatable](https://pub.dev/packages/equatable)
- [flutter_bloc](https://pub.dev/packages/flutter_bloc)
- [flutter_config](https://pub.dev/packages/flutter_config)
- [dio](https://pub.dev/packages/dio)
- [scale_size](https://pub.dev/packages/scale_size)
- [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)