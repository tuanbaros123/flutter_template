import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'localizations.dart';

extension AppResponse on Response {
  bool get isSuccess {
    return this.statusCode != null && this.statusCode == 200;
  }
}

extension GetTextLocale on BuildContext {
  String text(String key) {
    return Language.of(this)!.getText(key);
  }
}
