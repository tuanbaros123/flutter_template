import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:{{cookiecutter.flutter_package_name}}/blocs/base_bloc/base.dart';
import 'package:{{cookiecutter.flutter_package_name}}/data/repository/repository.dart';

class HomeCubit extends Cubit<BaseState> {
  final HomeRepository homeRepository;

  HomeCubit({required HomeRepository homeRepository})
      : this.homeRepository = homeRepository,
        super(InitState());

  getData() async {
    emit(LoadingState());
    try {
      //do something
      final data = await homeRepository.getData();
      emit(LoadedState<String>(data));
    } catch (e) {
      emit(ErrorState(e));
    }
  }
}
