import 'package:bloc/bloc.dart';

class SimpleBlocObserver extends BlocObserver {

  @override
  void onChange(BlocBase bloc, Change change) {
    print('onChange ----cubit -- : ${bloc.runtimeType}, change: $change');
    super.onChange(bloc, change);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print('onError ----cubit -- : ${bloc.runtimeType}, error: $error');
    super.onError(bloc, error, stackTrace);
  }
}
