import 'package:flutter_config/flutter_config.dart';

class ApiConstant {
  //define all api constant like: host, path, ...
  static final apiHost = "${FlutterConfig.get("API_HOST")}/";
}