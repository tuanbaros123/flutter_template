import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:{{cookiecutter.flutter_package_name}}/blocs/blocs.dart';
import 'package:{{cookiecutter.flutter_package_name}}/localizations.dart';
import 'package:{{cookiecutter.flutter_package_name}}/data/repository/repository.dart';
import 'package:{{cookiecutter.flutter_package_name}}/routes.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/base_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) => HomeCubit(homeRepository: HomeRepository())..getData(),
      child: Scaffold(
        body: Center(
          child: BlocBuilder<HomeCubit, BaseState>(
            builder: (context, state) {
              if (state is LoadingState) return CircularProgressIndicator();
              if (state is LoadedState) return Text(state.data.toString());
              return InkWell(child: Text(Language.of(context)!.getText("example")));
            },
          ),
        ),
      ),
    );
  }
}
