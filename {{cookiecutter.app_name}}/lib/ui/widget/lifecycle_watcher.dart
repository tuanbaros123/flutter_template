import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class LifecycleWatcher extends StatefulWidget {
  final Widget child;
  final bool whiteStatusBar;
  const LifecycleWatcher(
      {Key? key, required this.child, this.whiteStatusBar = false})
      : super(key: key);

  @override
  _LifecycleWatcherState createState() => _LifecycleWatcherState();
}

class _LifecycleWatcherState extends State<LifecycleWatcher>
    with WidgetsBindingObserver {
  static List<bool> stackData = [];

  @override
  void initState() {
    super.initState();
    // FlutterStatusbarcolor.setStatusBarWhiteForeground(widget.whiteStatusBar);
    stackData.add(widget.whiteStatusBar);
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    debugPrint("dispose");
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void deactivate() {
    debugPrint("deactivate");
    try {
      stackData.removeLast();
      // FlutterStatusbarcolor.setStatusBarWhiteForeground(stackData.last);
    } catch (e) {}
    super.deactivate();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    debugPrint("didChangeAppLifecycleState");
    // await FlutterStatusbarcolor.setStatusBarWhiteForeground(widget.whiteStatusBar);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: widget.whiteStatusBar ? Colors.red : Colors.transparent,
    ));
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // Use [SystemUiOverlayStyle.light] for white status bar
      // or [SystemUiOverlayStyle.dark] for black status bar
      // https://stackoverflow.com/a/58132007/1321917
      value: widget.whiteStatusBar
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark,
      child: widget.child,
    );
  }
}
