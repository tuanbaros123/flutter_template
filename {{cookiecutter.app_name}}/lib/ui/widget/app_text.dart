import 'package:flutter/material.dart';
import 'package:{{cookiecutter.flutter_package_name}}/extension.dart';
import 'widgets.dart';

class AppText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;
  final TextAlign align;
  final FontWeight fontWeight;
  final double? fontHeight;
  final FontStyle? fontStyle;
  final TextDecoration decoration;
  final int? maxLines;
  final TextStyle? style;
  final bool formatCurrency;
  final TextOverflow textOverflow;

  AppText(this.text,
      {this.fontSize = 16,
      this.color = Colors.black,
      this.align = TextAlign.left,
      this.fontWeight = FontWeight.normal,
      this.fontStyle = FontStyle.normal,
      this.fontHeight,
      this.decoration = TextDecoration.none,
      this.maxLines,
      this.style,
      this.formatCurrency = false,
      this.textOverflow = TextOverflow.visible});

  @override
  Widget build(BuildContext context) {
    return KeepFontSize(
      child: Text(
        context.text(text),
        textAlign: align,
        maxLines: this.maxLines,
        overflow: textOverflow,
        style: style == null
            ? TextStyle(
                fontSize: fontSize,
                fontWeight: fontWeight,
                color: color,
                height: fontHeight,
                fontStyle: fontStyle,
                decoration: decoration,
              )
            : style,
      ),
    );
  }
}
