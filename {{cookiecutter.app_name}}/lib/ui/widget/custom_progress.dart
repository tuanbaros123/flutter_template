import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:{{cookiecutter.flutter_package_name}}/blocs/blocs.dart';
import 'widgets.dart';

class CustomProgress<T extends Cubit<BaseState>> extends StatelessWidget {
  final bloc;
  final Widget? loadingWidget;
  final Color backgroundColor;

  const CustomProgress({Key? key, this.bloc, this.loadingWidget, this.backgroundColor = Colors.black12})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      right: 0,
      left: 0,
      bottom: 0,
      child: BlocBuilder<T, BaseState>(
        bloc: bloc,
        builder: (_, state) {
          if (state is LoadingState) {
            return Container(
              decoration: BoxDecoration(
                color: this.backgroundColor,
              ),
              child: Center(
                child: loadingWidget == null ? BaseProgressIndicator() : loadingWidget,
              ),
            );
          }
          return Container();
        },
      ),
    );
    // return BlocBuilder<T, BaseState>(
    //   builder: (_, state) {
    //     if (state is LoadingState) {
    //       return GestureDetector(
    //         onTap: () {},
    //         child: Container(
    //           decoration: BoxDecoration(
    //             color: Colors.black12,
    //           ),
    //           child: Center(
    //             child: CircularProgressIndicator(
    //               strokeWidth: 3,
    //               backgroundColor: Color(0xffb0934d),
    //               valueColor: new AlwaysStoppedAnimation<Color>(AppColors.MAIN_COLOR),
    //             ),
    //           ),
    //         ),
    //       );
    //     }
    //     return Wrap();
    //   },
    // );
  }
}
