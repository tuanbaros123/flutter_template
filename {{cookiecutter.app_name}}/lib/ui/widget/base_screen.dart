import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:{{cookiecutter.flutter_package_name}}/res/resources.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/widgets.dart';
import 'package:scale_size/scale_size.dart';

class BaseScreen extends StatelessWidget {
  static double toolbarHeight = 50.sw;
  // body của màn hình
  final Widget body;
  // title của appbar có 2 kiểu String và Widget
  // title là kiểu Widget thì sẽ render widget
  // title là String
  final dynamic title;
  // trường hợp có AppBar đặc biệt thì dùng customAppBar
  final Widget? customAppBar;
  // backgroundColor của AppBar mặc định là white
  final Color backgroundColor;
  // callBack của onBackPress với trường hợp  hiddenIconBack = false
  final Function? onBackPress;
  // custom widget bên phải của appBar
  final List<Widget> rightWidgets;
  // loadingWidget để show loading toàn màn hình
  final Widget? loadingWidget;
  // show thông báo
  final Widget? messageNotify;
  final Widget? floatingButton;
  // nếu true => sẽ ẩn backIcon , mặc định là true
  final bool hiddenIconBack;

  final Color colorTitle;
  final bool hideAppBar;
  final String? backgroundImage;

  const BaseScreen(
      {Key? key,
      required this.body,
      this.title = "",
      this.customAppBar,
      this.onBackPress,
      this.backgroundColor = Colors.transparent,
      this.rightWidgets = const [],
      this.hiddenIconBack = true,
      this.colorTitle = Colors.white,
      this.loadingWidget,
      this.hideAppBar = false,
      this.messageNotify,
      this.floatingButton,
      this.backgroundImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Scaffold scaffold = Scaffold(
        appBar: hideAppBar ? null : (customAppBar == null ? baseAppBar() : customAppBar),
        backgroundColor: backgroundColor,
        body: Container(
          decoration: BoxDecoration(color: backgroundColor),
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Stack(
              children: [
                body,
                Positioned(
                  top: AppDimens.SIZE_0,
                  right: AppDimens.SIZE_0,
                  left: AppDimens.SIZE_0,
                  bottom: AppDimens.SIZE_0,
                  child: loadingWidget ?? Container(),
                ),
                messageNotify ?? Container()
              ],
            ),
          ),
        ),
        floatingActionButton: floatingButton ?? null);
    Widget child = scaffold;
    if (backgroundImage != null) {
      child = Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(backgroundImage!),
                fit: BoxFit.cover
              )
            ),
          ),
          scaffold
        ],
      );
    }
    return child;
  }

  baseAppBar() {
    var widgetTitle;
    if (title is Widget) {
      widgetTitle = title;
    } else {
      String title = this.title ?? "";
      widgetTitle = AppText(
        title,
        maxLines: 2,
        fontWeight: FontWeight.w600,
        fontSize: 20.sw,
        align: TextAlign.center,
        fontHeight: 26 / 22,
        color: colorTitle,
      );
    }
    return AppBar(
      elevation: 0,
      toolbarHeight: toolbarHeight,
      backgroundColor: backgroundColor,
      title: widgetTitle,
      leading: hiddenIconBack
          ? Container()
          : InkWell(
              onTap: () {
                onBackPress!();
              },
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
            ),
      centerTitle: true,
      actions: rightWidgets,
    );
  }
}
