import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:{{cookiecutter.flutter_package_name}}/blocs/blocs.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/app_text.dart';

class CustomSnackBar<T extends Cubit<BaseState>> extends StatelessWidget {
  final double? fontSize;
  final Color? textColor;
  final cubit;

  const CustomSnackBar({Key? key, this.fontSize, this.textColor, this.cubit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<T, BaseState>(
      bloc: this.cubit,
        child: Container(),
        listener: (context, state) {
          String? mess;
          if (state is LoadedState && (state.msgError.isNotEmpty)) {
            mess = state.msgError;
          } else if (state is ErrorState) {
            mess = state.data.toString();
          }
          if (mess != null && mess.isNotEmpty) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: AppText(
                mess,
                fontSize: fontSize ?? 14,
                maxLines: 3,
                color: textColor ?? Colors.black,
                fontWeight: FontWeight.w400,
              ),
              behavior: SnackBarBehavior.floating,
              margin: EdgeInsets.all(20),
              backgroundColor: Colors.white,
              duration: Duration(milliseconds: 2000),
            ));
          }
        });
  }
}
