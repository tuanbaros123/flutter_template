import 'package:flutter/material.dart';

class KeepStateWidget extends StatefulWidget {
  final Widget child;

  const KeepStateWidget({Key? key, required this.child}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _KeepState();
  }
}

class _KeepState extends State<KeepStateWidget> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }
}
