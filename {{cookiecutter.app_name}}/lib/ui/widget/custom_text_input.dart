import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../localizations.dart';

class CustomTextInput extends StatefulWidget {
  final onSubmitted;
  final TextInputType keyboardType;
  final String title;
  final TextStyle? titleStyle;
  final int maxLines;
  final TextInputAction? textInputAction;
  final String errorText;
  final Function? getTextFieldValue;
  final int minLines;
  final Function? changeFocus;
  final String hintText;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final initData;
  final double? width;
  final TextEditingController? textController;
  final TextAlign? align;
  final bool enabled;
  final hideUnderline;
  final formatNumber;
  final TextStyle? style;
  final int maxLength;
  final formatPercent;
  final bool formatDecimal;
  final bool isPassword;
  final FocusNode? focusNode;
  final Color textColor;
  final Color bgColor;
  final Widget? prefixIcon;

  CustomTextInput({
    Key? key,
    this.getTextFieldValue,
    this.onSubmitted,
    this.keyboardType = TextInputType.text,
    this.title = "",
    this.maxLines = 1,
    this.textInputAction,
    this.errorText = "",
    this.minLines = 1,
    this.changeFocus,
    this.hintText = "",
    this.margin,
    this.padding,
    this.initData,
    this.titleStyle,
    this.width,
    this.textController,
    this.align,
    this.enabled = true,
    this.hideUnderline = false,
    this.style,
    this.formatNumber = false,
    this.maxLength = TextField.noMaxLength,
    this.formatPercent = false,
    this.formatDecimal = false,
    this.isPassword = false,
    this.focusNode,
    this.textColor = Colors.black,
    this.bgColor = Colors.transparent,
    this.prefixIcon,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TextFieldState();
  }
}

class TextFieldState extends State<CustomTextInput> {
  late bool _passwordVisible;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    if (widget.initData != null) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        widget.textController!.text = widget.initData.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width ?? double.infinity,
      margin: widget.margin ?? EdgeInsets.zero,
      child: Wrap(
        children: [
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(100), color: widget.bgColor),
            child: TextField(
              focusNode: widget.focusNode,
              maxLength: widget.maxLength,
              cursorColor: widget.textColor,
              enabled: widget.enabled,
              textAlign: widget.align ?? TextAlign.start,
              style: widget.style ?? TextStyle(color: widget.textColor, fontSize: 14, fontWeight: FontWeight.normal),
              decoration: InputDecoration(
                counterText: "",
                focusColor: Colors.white,
                border: InputBorder.none,
                disabledBorder: widget.hideUnderline
                    ? InputBorder.none
                    : UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                focusedBorder: widget.hideUnderline
                    ? InputBorder.none
                    : UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                enabledBorder: widget.hideUnderline
                    ? InputBorder.none
                    : UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                      ),
                hintStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
                hintText: Language.of(context)!.getText(widget.hintText),
                isDense: true,
                contentPadding: widget.padding ?? EdgeInsets.only(left: 0, top: 5, right: 0, bottom: 5),
                suffixIconConstraints: BoxConstraints(
                  maxWidth: widget.isPassword ? 35 : 0,
                  maxHeight: 24,
                ),
                suffixIcon: widget.isPassword
                    ? IconButton(
                        padding: EdgeInsets.all(4),
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          _passwordVisible ? Icons.visibility_off : Icons.visibility,
                          color: Colors.grey,
                          size: 16,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      )
                    : Container(),
                prefixIcon: widget.prefixIcon ?? Container(width: 0),
                prefixIconConstraints: BoxConstraints(
                  maxWidth: 30,
                  maxHeight: 24,
                ),
              ),
              controller: widget.textController,
              obscureText: widget.isPassword ? !_passwordVisible : false,
              keyboardType: widget.keyboardType,
              textInputAction: widget.textInputAction,
              onSubmitted: widget.onSubmitted,
              onEditingComplete: () => FocusScope.of(context).nextFocus(),
              maxLines: widget.maxLines,
              minLines: widget.minLines,
              onTap: () {
                if (widget.textController!.text == '0') {
                  widget.textController!.selection =
                      TextSelection.fromPosition(TextPosition(offset: widget.textController!.text.length));
                }
              },
              onChanged: (_text) {
                String currentText = _text.trim();
                widget.getTextFieldValue!(currentText);
              },
            ),
          ),
          widget.errorText != ""
              ? Container(
                  margin: EdgeInsets.only(top: 6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.error,
                        size: 13,
                        color: Colors.red,
                      ),
                      SizedBox(width: 2),
                      Expanded(
                        child: Text(
                          Language.of(context)!.getText(widget.errorText),
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        ),
                      ),
                    ],
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
