import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'base_progress_indicator.dart';
import 'package:scale_size/scale_size.dart';

class BaseNetworkImage extends StatelessWidget {
  final String? url;
  final double? borderRadius;
  final double? width;
  final double? height;
  final String? errorAssetImage;
  final double? loadingSize;
  final BoxFit? boxFit;

  const BaseNetworkImage(
      {Key? key,
      this.url,
      this.borderRadius,
      this.width,
      this.height,
      this.errorAssetImage,
        // add default error image if need
      // this.errorAssetImage = AppImages.ERROR_IMAGE,
      this.loadingSize,
      this.boxFit = BoxFit.cover})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget errorWidget = ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius ?? 10),
      child: Container(
        width: this.width ?? double.infinity,
        height: this.height ?? double.infinity,
        color: Colors.black,
        child: Image.asset(
          errorAssetImage!,
          width: this.width,
          height: this.height,
          fit: BoxFit.cover,
        ),
      ),
    );
    return url == null
        ? errorWidget
        : Container(
            width: this.width,
            height: this.height,
            // color: Colors.white,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(borderRadius ?? 10.sw),
              child: CachedNetworkImage(
                placeholder: (_, __) => Center(
                  child: loadingSize == null
                      ? BaseProgressIndicator()
                      : BaseProgressIndicator(
                    size: loadingSize,
                  ),
                ),
                imageUrl: url ?? "",
                width: this.width,
                height: this.height,
                fit: boxFit,
                errorWidget: (_, __, ___) {
                  return errorWidget;
                },
              ),
            ),
          );
  }
}
