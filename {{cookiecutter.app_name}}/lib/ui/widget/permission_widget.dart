import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionWidget extends StatefulWidget {
  final Permission permission;
  final Widget deniedWidget;
  final Widget grantedWidget;
  final PermissionController controller;

  const PermissionWidget(
      {Key? key, required this.permission, required this.deniedWidget, required this.grantedWidget, required this.controller})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PermissionWidgetState();
  }
}

class PermissionController extends ChangeNotifier {
  request() {
    notifyListeners();
  }
}

class _PermissionWidgetState extends State<PermissionWidget> with WidgetsBindingObserver {
  late PermissionStatus _status;

  @override
  void initState() {
    widget.controller.addListener(_requestPermission);
    WidgetsBinding.instance!.addObserver(this);
    widget.permission.status.then((value) {
      setState(() {
        _status = value;
        // if (value == PermissionStatus.granted && widget.scanController != null) {
        //   widget.scanController.start();
        // }
      });
    });
    super.initState();
  }

  _requestPermission() {
    if (Platform.isAndroid && _status == PermissionStatus.permanentlyDenied) {
      openAppSettings();
      return;
    }
    if (Platform.isIOS && _status == PermissionStatus.denied) {
      openAppSettings();
      return;
    }
    widget.permission.request().then((value) {
      print(value);
      setState(() {
        _status = value;
      });
      // if (value == PermissionStatus.granted && widget.scanController != null) {
      //   widget.scanController.start();
      // }
    });
  }

  @override
  void dispose() {
    widget.controller.removeListener(_requestPermission);
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  // check permissions when app is resumed
  // this is when permissions are changed in app settings outside of app
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      widget.permission.status.then((value) {
        print(value);
        setState(() {
          _status = value;
        });
        // if (value == PermissionStatus.granted && widget.scanController != null && widget.scanController.isScanActive) {
        //   widget.scanController.start();
        // }
      });
    } else if (state == AppLifecycleState.inactive) {
      // if (widget.scanController != null) {
      //   widget.scanController.stop();
      // }
    }
  }

  @override
  Widget build(BuildContext context) {
    return _status == PermissionStatus.granted ? widget.grantedWidget : widget.deniedWidget;
  }
}
