import 'package:flutter/material.dart';

class KeepFontSize extends StatelessWidget {
  final Widget child;

  const KeepFontSize({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: child,
    );
  }
}
