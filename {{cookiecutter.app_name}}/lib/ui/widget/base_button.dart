import 'package:flutter/material.dart';

class BaseButton extends StatelessWidget {
  final BoxDecoration? decoration;
  final GestureTapCallback? onTap;
  final Widget child;
  final Color backgroundColor;
  final double borderRadius;
  final Color borderColor;
  final EdgeInsetsGeometry margin;
  final EdgeInsetsGeometry padding;

  const BaseButton(
      {Key? key,
      required this.child,
      this.decoration,
      this.onTap,
      this.backgroundColor = Colors.transparent,
      this.borderRadius = 0,
      this.borderColor = Colors.transparent,
      this.margin = EdgeInsets.zero,
      this.padding = EdgeInsets.zero})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(borderRadius),
        onTap: () {
          onTap!();
        },
        child: Container(
          margin: this.margin,
          padding: this.padding,
          alignment: Alignment.center,
          decoration: decoration ??
              BoxDecoration(
                  color: this.backgroundColor,
                  border: Border.all(color: borderColor),
                  borderRadius: BorderRadius.circular(borderRadius)),
          child: child,
        ),
      ),
    );
  }
}
