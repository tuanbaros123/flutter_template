import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:{{cookiecutter.flutter_package_name}}/res/resources.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/app_text.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/keep_font_size.dart';
import 'package:scale_size/scale_size.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController? textEditingController;
  final Function? onSubmit;
  final Function? onChanged;
  final EdgeInsets? margin;
  final String? hintText;
  final Widget? rightWidget;
  final Widget? leftWidget;
  final Color? backgroundColor;
  final List<BoxShadow>? boxShadow;
  final String? title;
  final String errorText;
  final bool needShowClearAll;
  final bool? enabled;
  final bool? autofocus;
  final double? height;
  final int maxLines;
  final Function? onClear;

  const CustomTextField(
      {Key? key,
      this.onSubmit,
      @required this.textEditingController,
      this.hintText,
      this.onChanged,
      this.margin,
      this.rightWidget,
      this.leftWidget,
      this.backgroundColor,
      this.boxShadow,
      this.title,
      this.needShowClearAll = false,
      this.height,
      this.enabled,
      this.autofocus,
      this.maxLines = 1,
      this.onClear,
      this.errorText = ""})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late bool isShowClearAll;

  @override
  void initState() {
    super.initState();
    isShowClearAll = widget.textEditingController!.text.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (widget.title?.isNotEmpty ?? false)
            ? Container(
                margin: EdgeInsets.only(bottom: 8.sh),
                child: AppText(
                  widget.title ?? "",
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              )
            : Container(),
        Container(
          height: widget.height ?? 48.sh,
          margin: widget.margin ?? EdgeInsets.zero,
          decoration: BoxDecoration(
            boxShadow: widget.boxShadow ?? [],
            color: widget.backgroundColor ?? Colors.white,
            borderRadius: BorderRadius.circular(5.sw),
          ),
          child: Row(
            children: [
              widget.leftWidget ?? Container(),
              SizedBox(width: 10.sw),
              Expanded(
                  child: Container(
                // alignment: widget.height == null ? Alignment.center : Alignment.topRight,
                child: KeepFontSize(
                  child: TextField(
                    maxLines: widget.maxLines,
                    enabled: widget.enabled ?? true,
                    autofocus: widget.autofocus ?? false,
                    cursorColor: Colors.green,
                    controller: widget.textEditingController,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        isDense: true,
                        hintText: widget.hintText ?? "",
                        hintStyle: TextStyle(fontSize: 16.sw, fontWeight: FontWeight.w400, height: 21 / 18)),
                    style: TextStyle(
                        color: Colors.black, fontSize: 16.sw, fontWeight: FontWeight.w400, height: 21 / 18),
                    onChanged: (text) {
                      widget.onChanged!(text);
                      setState(() {
                        isShowClearAll = text.isNotEmpty;
                      });
                    },
                    onSubmitted: (text) {
                      widget.onSubmit!(text);
                    },
                  ),
                ),
              )),
              isShowClearAll && widget.needShowClearAll == true
                  ? GestureDetector(
                      child: Container(
                        width: 30.sw,
                        alignment: Alignment.center,
                        // add IC_CLOSE and uncomment
                        // child: Image.asset(
                        //   AppImages.IC_CLOSE,
                        //   color: Colors.black,
                        //   fit: BoxFit.fill,
                        // ),
                      ),
                      onTap: () {
                        widget.textEditingController!.clear();
                        widget.onClear!();
                        setState(() {
                          isShowClearAll = false;
                        });
                        widget.onChanged!("");
                      },
                    )
                  : SizedBox(width: 10.sw),
              widget.rightWidget ?? Container()
            ],
          ),
        ),
       ( widget.errorText.isNotEmpty)
            ? Container(
                margin: EdgeInsets.only(top: 6.sw),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.error,
                      size: AppDimens.SIZE_13,
                      color: Colors.red,
                    ),
                    SizedBox(width: 2),
                    Expanded(
                      child: AppText(widget.errorText, color: Colors.red, fontSize: AppDimens.SIZE_12),
                    ),
                  ],
                ),
              )
            : Container()
      ],
    );
  }
}
