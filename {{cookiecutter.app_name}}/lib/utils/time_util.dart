import 'package:intl/intl.dart';

class TimeUtil {
  static const String DEFAULT_FORMAT = "dd/MM/yyyy";

  static DateTime? parserDate(String date, {String format = DEFAULT_FORMAT}) {
    try {
      DateTime dateTime = DateFormat(format).parse(date);
      return dateTime;
    } catch (e) {
      return null;
    }
  }

  static String? fromDate(DateTime date, {String format = DEFAULT_FORMAT}) {
    try {
      String dateString = DateFormat(format).format(date.toLocal());
      return dateString;
    } catch (e) {
      return null;
    }
  }
}
