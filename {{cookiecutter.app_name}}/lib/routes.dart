import 'package:flutter/material.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/screen/screens.dart';
import 'package:{{cookiecutter.flutter_package_name}}/ui/widget/widgets.dart';

class Routes {
  Routes._();

  //screen name
  static const String homeScreen = "/homeScreen";

  //init screen name
  static String initScreen() => homeScreen;

  static final routes = <String, WidgetBuilder>{
    homeScreen: (context) => LifecycleWatcher(child: HomeScreen(), whiteStatusBar: false,),
  };
}
